#include <vcc.h>

int is_multiple(int x, int y)
_(ensures \result == 1 || \result == 0)
{
  if (x != 0) {
    if (y % x == 0)
    {
      return 1; 
    }
    else
    {
      return 0;
    } 
  }
  else {
    return 0;
  }
}

void is_arr_multiple(int x, int* arrIn, int* arrOut, unsigned size)
_(requires \thread_local_array(arrIn, size))
_(writes \array_range(arrOut,size))
_(ensures \forall unsigned i; 
    i < size ==> (arrOut[i] == 0 || arrOut[i] == 1))
{
  unsigned i;
  for (i = 0; i < size; i++)
  _(invariant \forall unsigned j; 
    j < i ==> (arrOut[j] == 0 || arrOut[j] == 1))
  {
    arrOut[i] = is_multiple(x, arrIn[i]);
  }
}



int main()
{
  int arrayIn[5] = {2,5,10};
  int arrayOut[5];
  
  is_arr_multiple(50, arrayIn, arrayOut, 3);
  
  return 0;
  
} 